class Arbol {

    constructor() {
        this.nodoPadre = this.agregarNodoPadre();
        this.nivel = 3;
        //this.elemntoBusqueda=100;
        this.buscarNodos = [];
    }

    agregarNodoPadre(){
        var nodo = new Nodo(null,null,"+",100);
        return nodo;
    }

    agregarNodo(nodoPadre,posicion,nombre,valor){
        var nodo = new Nodo(nodoPadre,posicion,nombre,valor);
        return nodo;
    }

    verificarNivelHijos(nodo){

        if(nodo.nivel == this.nivel)
            this.buscarNodos.push(nodo.nombre);

        if(nodo.hasOwnProperty('hI'))
            this.verificarNivelHijos(nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.verificarNivelHijos(nodo.hD);

        return this.buscarNodos;

    }
    buscarValor(buscarElemento,nodo=this.nodoPadre){

        if(nodo.valor == buscarElemento)
            return nodo;

        if(nodo.hasOwnProperty('hI'))
            this.verificarNivelHijos(nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.verificarNivelHijos(nodo.hD);



    }
}
