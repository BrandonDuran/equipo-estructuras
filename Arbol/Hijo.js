class Hijo{
    constructor(tipo,valor,nivel,padre,hijoDerecho,hijoIzquierdo) {
        this.tipo=tipo;
        this.valor=valor;
        this.nivel=nivel;
        this.padre=padre;
        this.hijoDerecho=hijoDerecho;
        this.hijoIzquierdo=hijoIzquierdo;

        if(hijoDerecho=="d")
            this.HijoDerecho=this.crearHijo("d",0,nivel+1,1,0,0,this.nivel);
        if(hijoIzquierdo=="-")
            this.HijoIzquierdo = this.crearHijo("-", 0, nivel + 1, 1,"*","a", this.nivel);

    }
    crearHijo(tipo,valor,nivel,padre,hijoDerecho,hijoIzquierdo){
        var CrearHijo=new Hijo(tipo,valor,nivel,padre,hijoDerecho,hijoIzquierdo);
        return CrearHijo;
    }
}