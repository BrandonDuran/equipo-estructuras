class Nodo{
constructor(nombre,nivel,padre,Hl,HR) {
    this.nombre=nombre;
    this.nivel=nivel;
    this.padre=padre;
    this.HL=Hl;
    this.HR=HR;
    if(Hl==9)
        this.HL=this.crearHijo(9,nivel+1,padre+1,6,14);
    if(HR==20)
        this.HR=this.crearHijo(20,nivel+1,padre+1,17,64)
    if(Hl==6)
        this.HL=this.crearHijo(6,nivel+1,padre+1,0,0);
    if(HR==14)
        this.HR=this.crearHijo(14,nivel+1,padre+1,13,0);
    if(Hl==13)
        this.HL=this.crearHijo(13,nivel+1,padre+1,0,0);
    if(Hl==17)
        this.HL=this.crearHijo(17,nivel+1,padre+1,0,0);
    if(HR==64)
        this.HR=this.crearHijo(64,nivel+1,padre+1,26,72);
    if(Hl==26)
        this.HL=this.crearHijo(26,nivel+1,padre+1,0,0);
    if(HR==72)
        this.HR=this.crearHijo(72,nivel+1,padre+1,0,0);
}
crearHijo(nombre,nivel,padre,HL,HR){
    var crearHijo=new Nodo(nombre,nivel,padre,HL,HR);
    return crearHijo;
}
}